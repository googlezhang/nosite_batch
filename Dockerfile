FROM hub.c.163.com/library/java:8-alpine

MAINTAINER nosite.cn sunyuguo@hotmail.com

Add ./batch-server-1.0.0.jar app.jar

EXPOSE 18086

ENTRYPOINT ["java","-jar","/app.jar"]