package com.nosite.batch.server.batch;

import com.nosite.batch.server.listener.AutoCreateProductTaskletListener;
import com.nosite.batch.server.tasklet.AutoCreateProductTasklet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: SunYuGuo
 * @Date: 2019/11/17 21:40
 * @Description:
 */
@Configuration
@Slf4j
public class JobConfiguration {

    //注入创建任务对象的对象
    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    //任务的执行由Step决定
    //注入创建Step对象的对象
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private AutoCreateProductTasklet autoCreateProductTasklet;

    @Autowired
    private AutoCreateProductTaskletListener autoCreateProductTaskletListener;

    //创建任务对象
    @Bean
    public Job autoCreateProductJob(){
        return jobBuilderFactory.get("autoCreateProductJob").start(autoCreateProductStep()).build();
    }
    @Bean
    public Step autoCreateProductStep() {
        return stepBuilderFactory.get("autoCreateProductStep")
                .listener(autoCreateProductTaskletListener)
                .tasklet(autoCreateProductTasklet).build();
    }
}
