package com.nosite.batch.server.quartz;

import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: SunYuGuo
 * @Date: 2019/11/17 21:41
 * @Description:
 */
@Configuration
public class QuartzConfiguration {

    //自动注入进来的是SimpleJobLauncher
    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobLocator jobLocator;

    /*用来注册job*/
    /*JobRegistry会自动注入进来*/
    @Bean
    public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor(JobRegistry jobRegistry){
        JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor = new JobRegistryBeanPostProcessor();
        jobRegistryBeanPostProcessor.setJobRegistry(jobRegistry);
        return jobRegistryBeanPostProcessor;
    }

    @Bean
    public JobDetailFactoryBean jobDetailFactoryBean(){
        JobDetailFactoryBean jobFactory = new JobDetailFactoryBean();
        jobFactory.setJobClass(QuartzJobLauncher.class);
        jobFactory.setGroup("sellBatchGroup");
        jobFactory.setName("sellBatchJob");
        Map<String, Object> map = new HashMap<>();
        map.put("jobName", "autoCreateProductJob");
        map.put("jobLauncher", jobLauncher);
        map.put("jobLocator", jobLocator);
        jobFactory.setJobDataAsMap(map);
        return jobFactory;
    }

    @Bean
    public CronTriggerFactoryBean cronTriggerFactoryBean(){
        CronTriggerFactoryBean cTrigger = new CronTriggerFactoryBean();
        cTrigger.setJobDetail(jobDetailFactoryBean().getObject());
        cTrigger.setStartDelay(5000);
        cTrigger.setName("sellBatchTrigger");
        cTrigger.setGroup("sellBatchGroup");
        cTrigger.setCronExpression("0 0 0 * * ?");
        return cTrigger;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(){
        SchedulerFactoryBean schedulerFactor = new SchedulerFactoryBean();
        schedulerFactor.setTriggers(cronTriggerFactoryBean().getObject());
        return schedulerFactor;
    }

}
