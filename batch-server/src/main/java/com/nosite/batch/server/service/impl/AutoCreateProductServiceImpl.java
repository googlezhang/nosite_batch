package com.nosite.batch.server.service.impl;

import com.nosite.batch.common.dao.ProductCategoryMapper;
import com.nosite.batch.common.dao.ProductInfoMapper;
import com.nosite.batch.common.po.ProductCategory;
import com.nosite.batch.common.po.ProductInfo;
import com.nosite.batch.server.service.intf.AutoCreateProductService;
import com.nosite.batch.server.util.ReptileData;
import com.nosite.batch.server.util.SnowflakeIdWorker;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Auther: SunYuGuo
 * @Date: 2019/11/17 22:01
 * @Description:
 */
@Service
@Slf4j
public class AutoCreateProductServiceImpl implements AutoCreateProductService {

    @Autowired
    SnowflakeIdWorker snowflakeIdWorker;
    @Autowired
    ReptileData reptileData;
    @Autowired
    private ProductInfoMapper productInfoMapper;
    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Value("${reptile.url}")
    private String url;

    /**
     * 自动创建商品
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void autoCreateProduct() {
        List<ProductCategory> productCategoryList = productCategoryMapper.selectAll();
        List<ProductInfo> productInfoList = new ArrayList<ProductInfo>();
        for (ProductCategory productCategory : productCategoryList) {
            Elements elements = reptileData.reptileAdapter(productCategory.getCategoryName(),url);
           if (elements != null){
            for (Element item : elements) {
                //商品名称
                String productName = item.select("p[class='productTitle']").select("a").attr("title");
                //商品价格
                String productPrice = item.select("p[class='productPrice']").select("em").attr("title");
                //商品网址
                String goodsUrl = item.select("p[class='productTitle']").select("a").attr("href");
                //商品图片网址
                String productIcon = item.select("div[class='productImg-wrap']").select("a").select("img").attr("data-ks-lazyload");
                if (!StringUtils.isEmpty(productName) && !StringUtils.isEmpty(productPrice)
                        && !StringUtils.isEmpty(productIcon)) {
                    ProductInfo productInfo = new ProductInfo();
                    long productId = snowflakeIdWorker.nextId();
                    productInfo.setProductId(String.valueOf(productId));
                    productInfo.setProductName(productName);
                    productInfo.setProductPrice(new BigDecimal(productPrice));
                    productInfo.setProductIcon("https:"+productIcon);
                    productInfo.setCategoryType(productCategory.getCategoryId());
                    productInfo.setProductStock(100);
                    productInfo.setProductDescription(productInfo.getProductName());
                    productInfo.setProductStatus(0);
                    productInfo.setCreateTime(new Date());
                    productInfo.setUpdateTime(new Date());
                    productInfoList.add(productInfo);
                }
            }
           }
        }

        //找出已经爬过的商品
        List<ProductInfo> reCreateProductInfo = productInfoMapper.selectByProductName(productInfoList);
        Map<String, ProductInfo> productInfoMap = reCreateProductInfo.stream()
                .collect(Collectors.toMap(ProductInfo::getProductName, productInfo -> productInfo));
        //找出没有爬过的商品
        List<ProductInfo> productInfos = productInfoList.stream().filter(productInfo -> productInfoMap.get(productInfo.getProductName()) == null)
                .collect(Collectors.toList());
        //批量入库
        productInfoMapper.insertBatch(productInfos);
    }
}
