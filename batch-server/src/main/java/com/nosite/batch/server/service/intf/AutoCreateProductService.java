package com.nosite.batch.server.service.intf;

/**
 * @Auther: SunYuGuo
 * @Date: 2019/11/17 22:01
 * @Description:
 */
public interface AutoCreateProductService {
    /**
     * 自动创建商品
     */
    public void autoCreateProduct();
}
