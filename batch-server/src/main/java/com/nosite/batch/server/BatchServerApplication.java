package com.nosite.batch.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.nosite.batch.*"})
@MapperScan("com.nosite.batch.common.dao")
public class BatchServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatchServerApplication.class, args);
    }

}
