package com.nosite.batch.server.util;

import freemarker.template.Template;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Map;
import java.util.Set;

/**
 * @Auther: SunYuGuo
 * @Date: 2019/11/24 17:10
 * @Description:
 */
@Component
@Data
@Slf4j
public class MailSenderService {

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;
    @Autowired
    private MailProperties mailProperties;
    /**
     * 收件人邮箱地址
     */
    private Set<String> toSet;
    /**
     * 抄送人邮箱地址
     */
    private Set<String> ccSet;
    /**
     * 邮件主题
     */
    private String subject;
    /**
     * 邮件内容
     */
    private String content;
    /**
     * 模版名称
     */
    private String templateName;

    /**
     * 发送模板邮件
     */
    public void sendWithTemplate(Map params) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(toSet != null ? toSet.toArray(new String[]{}) : new String[]{});
        simpleMailMessage.setCc(ccSet != null ? ccSet.toArray(new String[]{}) : new String[]{});
        simpleMailMessage.setFrom(mailProperties.getUsername());
        simpleMailMessage.setSubject(subject);
        String mailText = "";
        try {
            //通过指定模板名获取FreeMarker模板实例
            Template template = freeMarkerConfigurer.getConfiguration().getTemplate(templateName);
            //FreeMarker通过Map传递动态数据
            //注意动态数据的key和模板标签中指定的属性相匹配
            //解析模板并替换动态数据，最终code将替换模板文件中的${code}标签。
            mailText = FreeMarkerTemplateUtils.processTemplateIntoString(template, params);
        } catch (Exception e) {
            log.error("发送模板邮件发生异常",e);
            e.printStackTrace();
        }
        simpleMailMessage.setText(mailText);
        mailSender.send(simpleMailMessage);
    }

    /**
     * 发送普通文本邮件
     */
    public void sendText() {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(toSet != null ? toSet.toArray(new String[]{}) : new String[]{});
        simpleMailMessage.setCc(ccSet != null ? ccSet.toArray(new String[]{}) : new String[]{});
        simpleMailMessage.setFrom(mailProperties.getUsername());
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(content);
        mailSender.send(simpleMailMessage);
    }

    /**
     * 发送普通Html邮件
     */
    public void sendHtml(Map params) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
        try {
            messageHelper.setTo(toSet != null ? toSet.toArray(new String[]{}) : new String[]{});
            messageHelper.setCc(ccSet != null ? ccSet.toArray(new String[]{}) : new String[]{});
            messageHelper.setFrom(mailProperties.getUsername());
            messageHelper.setSubject(subject);
            String mailText = "";
            //通过指定模板名获取FreeMarker模板实例
            Template template = freeMarkerConfigurer.getConfiguration().getTemplate(templateName);
            //FreeMarker通过Map传递动态数据
            //注意动态数据的key和模板标签中指定的属性相匹配
            //解析模板并替换动态数据，最终code将替换模板文件中的${code}标签。
            mailText = FreeMarkerTemplateUtils.processTemplateIntoString(template, params);
            messageHelper.setText(mailText, true);
        } catch (Exception e) {
            log.error("发送普通Html邮件发生异常",e);
            e.printStackTrace();
        }
        mailSender.send(mimeMessage);
    }

    /**
     * 发送普通带一张图片的Html邮件
     */
    public void sendHtmlWithImage(String imagePath) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setTo(toSet != null ? toSet.toArray(new String[]{}) : new String[]{});
            messageHelper.setCc(ccSet != null ? ccSet.toArray(new String[]{}) : new String[]{});
            messageHelper.setFrom(mailProperties.getUsername());
            messageHelper.setSubject(subject);
            messageHelper.setText(content, true);
            FileSystemResource img = new FileSystemResource(new File(imagePath));
            messageHelper.addInline("image", img);
        } catch (MessagingException e) {
            log.error("发送普通带一张图片的Html邮件发生异常",e);
            e.printStackTrace();
        }
        mailSender.send(mimeMessage);
    }

    /**
     * 发送普通带附件的Html邮件
     */
    public void sendHtmlWithAttachment(String filePath) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setTo(toSet != null ? toSet.toArray(new String[]{}) : new String[]{});
            messageHelper.setCc(ccSet != null ? ccSet.toArray(new String[]{}) : new String[]{});
            messageHelper.setFrom(mailProperties.getUsername());
            messageHelper.setSubject(subject);
            messageHelper.setText(content, true);
            FileSystemResource file = new FileSystemResource(new File(filePath));
            messageHelper.addAttachment(file.getFilename(), file);
        } catch (MessagingException e) {
            log.error("发送普通带附件的Html邮件发生异常",e);
            e.printStackTrace();
        }
        mailSender.send(mimeMessage);
    }
}
