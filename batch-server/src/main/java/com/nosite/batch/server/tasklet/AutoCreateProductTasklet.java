package com.nosite.batch.server.tasklet;

import com.nosite.batch.server.service.intf.AutoCreateProductService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Auther: SunYuGuo
 * @Date: 2019/11/17 21:36
 * @Description:
 */
@Component
public class AutoCreateProductTasklet implements Tasklet {

    @Autowired
    private AutoCreateProductService autoCreateProductService;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        autoCreateProductService.autoCreateProduct();
        return RepeatStatus.FINISHED;
    }
}
