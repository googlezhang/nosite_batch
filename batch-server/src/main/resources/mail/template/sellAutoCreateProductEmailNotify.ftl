<html>
<body>
<div style="font-size:18px">
    <div>
        您好：</br>
        &nbsp;&nbsp;&nbsp;电商后台批处理系统，${date}&nbsp;发现<span style="color: red; ">自动下单批处理发生异常</span>。详细信息如下：${errObject}</br>
           请及时登录系统进行处理。</br>
    </div>
    </br>
    （该邮件为电商后台批处理系统自动发送，请勿回复！）</br>

    谢谢！
</div>
</body>
</html>